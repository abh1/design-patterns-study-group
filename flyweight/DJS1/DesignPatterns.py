# -*- coding: utf-8 -*-
"""
Created on Mon Nov 09 11:15:36 2015

Some useful design patterns


@author: DJS1
"""

class Flyweight(type):
    """
    Flyweight Mixin - to implement the Flyweight pattern
    To use: add __metaclass__ = Flyweight after the class declaration
    
    If the __init__ arguements are the same as a previous object then 
    the old object is returned instead and the __init__ function is NOT run
    
    """
    def __init__(cls, name, bases, dct):

        super(Flyweight, cls).__init__(name, bases, dct)

#        type.__init__(cls, *args, **kargs)

        # Create dictionary of objects already created        
        cls.__instances = dict()
        # Keep a separate reference to the original __new__ function 
        cls.__original_new = cls.__new__
        # Replace the __new__ function
        cls.__new__ = Flyweight._get_instance
        
        # Keep a separate reference to the __init-__ function that the user specified
        cls.__original_init = cls.__init__
        # Replace the init function with the replacement
        cls.__init__ = cls._replacementInit

    def _get_instance(cls, *args, **kargs):

        key = repr((args, tuple(kargs.items())))
        # Create new instance if required
        if  key in cls.__instances:
            obj = cls.__instances[key]
        else:
            obj= cls.__original_new(cls, *args, **kargs)
            cls.__instances[key] = obj
            # Run the original init from here. 
            cls.__original_init(obj, *args, **kargs)
        return obj

    @staticmethod
    def _replacementInit(obj, *args, **kargs):
        pass

class Singleton(type):
    """
    Singleton metaclass.
    To use: add __metaclass__ = Singleton after the class declaration
    """
    _instances = {}
    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]
        
        
# Quick test
if __name__ == '__main__':
    import numpy as np
    class TestFly(object):
        __metaclass__ = Flyweight
        
        def __init__(self, param):
            self.myValue = np.random.rand(1)[0]
            print "myValue = ", self.myValue
            print "my id = ", id(self), self
            
            
    a= TestFly(1)
    print 'a', a.myValue  
    b= TestFly(1)
    print 'b', b.myValue 
    c= TestFly(2)
    print 'c', c.myValue  
    
    print 'a', a.myValue    
    print 'b', b.myValue    
    print 'c', c.myValue    
    
    print a==b
    print a==c
    