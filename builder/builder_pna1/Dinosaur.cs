﻿using System;
using System.Text;

namespace Builder
{
    public class Dinosaur
    {
        private const string MutantGeneIcon = "@";
        private const string ParasolIcon = "-) ";
        private const string EggIcon = "O";
        private const string HornIcon = "^";
        private const string FurIcon = "#";
        private const string TailIcon = "~";
        private const string LegIcon = "| ";

        private const int MinInFront = 3;
        private const int MinSnout = 2;
        private const int MinBack = 3;

        private readonly int horns;
        private readonly int furs;
        private readonly int legs;
        private readonly int parasols;
        private readonly int tailLength;
        private readonly int eggs;
        private readonly int mutantGenes;

        public int Attack => this.horns;
        public int Movement => this.legs;
        public int Reproduction => this.eggs;
        public int Discount => this.mutantGenes;
        public int Initiative => this.tailLength;
        public int ColdResistance => this.furs;
        public int HeatResistance => this.parasols;

        public Dinosaur(
            int horns,
            int furs,
            int legs,
            int parasols,
            int tailLength,
            int eggs,
            int mutantGenes)
        {
            this.eggs = eggs;
            this.furs = furs;
            this.horns = horns;
            this.legs = legs;
            this.mutantGenes = mutantGenes;
            this.parasols = parasols;
            this.tailLength = tailLength;
        }

        public string GetStats()
        {
            return $"This dinosaur can:" + Environment.NewLine +
                $"- Survive from {ColdResistance} below to {HeatResistance} above ambient" + Environment.NewLine +
                $"- Do {Attack} damage" + Environment.NewLine +
                $"- Move {Movement} spaces" + Environment.NewLine +
                $"- Create {Reproduction} new dinosaurs" + Environment.NewLine +
                $"And has:" + Environment.NewLine +
                $"- {Initiative} iniative" + Environment.NewLine +
                $"- {Discount} discount on new cards";
        }

        public string GetImage()
        {
            var imageBuilder = new StringBuilder();

            var inFront = Math.Max(Math.Max(MinInFront, mutantGenes * MutantGeneIcon.Length), Math.Max(parasols * ParasolIcon.Length, eggs * EggIcon.Length));
            var snout = Math.Max(MinSnout, horns * HornIcon.Length);
            var back = Math.Max(MinBack, Math.Max(furs * FurIcon.Length, legs * LegIcon.Length - 1));

            imageBuilder.AppendLine();

            imageBuilder.Append(MutantGeneIcon, mutantGenes);
            imageBuilder.Append(' ', inFront - mutantGenes * MutantGeneIcon.Length);
            imageBuilder.Append(HornIcon, horns);
            imageBuilder.Append('-', snout - horns * HornIcon.Length);
            imageBuilder.AppendLine(@"0\");

            imageBuilder.Append(' ', inFront);
            imageBuilder.Append('_', snout);
            imageBuilder.AppendLine(@"  \");

            imageBuilder.Append(ParasolIcon, parasols);
            imageBuilder.Append(' ', snout + inFront - parasols * ParasolIcon.Length);
            imageBuilder.Append(@"\  ");
            imageBuilder.Append(FurIcon, furs);
            imageBuilder.Append('-', back - furs * FurIcon.Length);
            imageBuilder.Append(TailIcon, tailLength);
            imageBuilder.AppendLine();

            imageBuilder.Append(' ', inFront + snout + 1);
            imageBuilder.Append(@"\");
            imageBuilder.Append(' ', back + 1);
            imageBuilder.AppendLine("|");

            imageBuilder.Append(' ', inFront + snout + 2);
            imageBuilder.Append(@"\");
            imageBuilder.Append('_', back);
            imageBuilder.AppendLine("|");

            imageBuilder.Append(' ', snout + 3 + inFront);
            imageBuilder.Append(LegIcon, legs);
            imageBuilder.AppendLine();

            imageBuilder.Append(EggIcon, eggs);
            imageBuilder.Append(' ', snout + 3 + inFront - eggs * EggIcon.Length);
            imageBuilder.Append(LegIcon, legs);
            imageBuilder.AppendLine();

            return imageBuilder.ToString();
        }
    }

    public static class StringBuilderExtension
    {
        public static StringBuilder Append(this StringBuilder sb, string s, int repeats)
        {
            for (var i = 0; i < repeats; i++)
            {
                sb.Append(s);
            }
            return sb;
        }
    }
}
