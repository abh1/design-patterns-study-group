﻿using System;

namespace Builder
{
    class Program
    {
        static void Main(string[] args)
        {
            var running = true;
            var builder = new DinosaurBuilder();
            Console.WriteLine("Choose whether to add tail, an egg, a leg, fur, a parasol, a horn, or a mutant gene to your dinosaur. Alternatively, create the dinosaur you've designed, start a new dinosaur, or quit");
            while (running)
            {
                switch (Console.ReadLine())
                {
                    case "tail":
                        builder.GrowTail();
                        break;

                    case "egg":
                        builder.AddEgg();
                        break;

                    case "fur":
                        builder.AddFur();
                        break;

                    case "horn":
                        builder.AddHorn();
                        break;

                    case "parasol":
                        builder.AddParasol();
                        break;

                    case "gene":
                        builder.AddMutantGene();
                        break;

                    case "leg":
                        builder.AddLeg();
                        break;

                    case "new":
                        Console.WriteLine("Dinosaur discarded");
                        builder = new DinosaurBuilder();
                        break;

                    case "create":
                        var dino = builder.Create();
                        Console.WriteLine(dino.GetImage());
                        Console.WriteLine(dino.GetStats());
                        break;

                    case "quit":
                        running = false;
                        break;

                    default:
                        break;
                }
            }
        }
    }
}
