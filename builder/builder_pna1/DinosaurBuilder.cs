﻿namespace Builder
{
    public class DinosaurBuilder
    {
        private int horns = 0;
        private int furs = 1;
        private int legs = 1;
        private int parasols = 1;
        private int tailLength = 0;
        private int eggs = 1;
        private int mutantGenes = 0;

        public Dinosaur Create()
        {
            return new Dinosaur(horns, furs, legs, parasols, tailLength, eggs, mutantGenes);
        }

        public void AddLeg()
        {
            legs++;
        }

        public void AddHorn()
        {
            horns++;
        }

        public void AddFur()
        {
            furs++;
        }

        public void AddParasol()
        {
            parasols++;
        }

        public void GrowTail()
        {
            tailLength++;
        }

        public void AddEgg()
        {
            eggs++;
        }

        public void AddMutantGene()
        {
            mutantGenes++;
        }
    }
}
