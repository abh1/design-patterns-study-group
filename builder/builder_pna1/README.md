Slightly differently from the GoF example, the director knows what it�s building so I�m not taking advantage of the abstract part of the pattern. Instead, I�m using it as a way to create an immutable object in response to user/client input. It was inspired by part of a board game called Evo, which involves some very silly-looking dinosaurs.

Ah, should mention that you have to type �tail� to add a tail, �gene� for a mutant gene etc, and that �create� builds the dinosaur.
