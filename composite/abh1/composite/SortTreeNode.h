#ifndef SortTreeNode_H
#define SortTreeNode_H

class SortTreeNode {
    public:
        virtual bool insert(int value) = 0;
        virtual void print() = 0;
        virtual int getMinValue() = 0;
        virtual int getMaxValue() = 0;

};

#endif