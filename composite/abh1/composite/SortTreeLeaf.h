#ifndef SortTreeLeaf_H
#define SortTreeLeaf_H
#include "SortTreeNode.h"

class SortTreeLeaf : public SortTreeNode {
    public:
        SortTreeLeaf(int value);
        virtual bool insert(int value);
        virtual void print();
        virtual int getMinValue();
        virtual int getMaxValue();
    private:
        int value;
    
};
#endif