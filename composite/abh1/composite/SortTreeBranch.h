#ifndef SortTreeBranch_H
#define SortTreeBranch_H

#include "SortTreeNode.h"

class SortTreeBranch : public SortTreeNode {
    public:
        SortTreeBranch();
        SortTreeBranch(SortTreeNode* initial);
        ~SortTreeBranch();
        virtual bool insert(int value);
        virtual void print();
        virtual int getMinValue();
        virtual int getMaxValue();
    private:
        // if valueCount = 2 both branches are populated, if 1 only the left branch, if 0 neither
        int valueCount;
        SortTreeNode* left;
        SortTreeNode* right;
};
#endif