#include <stdio.h>
#include <stdlib.h>

#include "SortTreeBranch.h"
#include "SortTreeLeaf.h"

void printSortedList(SortTreeNode* composite) {
    composite->print();
    printf("\n");
}



int main(void) {
    SortTreeNode* tree = new SortTreeBranch();
    tree->insert(4);
    tree->insert(3);
    tree->insert(2);
    tree->insert(3);
    tree->insert(5);
    tree->insert(1);
    printSortedList(tree);
    delete tree;

    SortTreeNode* singleValue = new SortTreeLeaf(7);
    printSortedList(singleValue);
    delete singleValue;

	return EXIT_SUCCESS;
}
