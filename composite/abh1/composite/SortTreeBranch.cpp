#include <stdio.h>

#include "SortTreeBranch.h"
#include "SortTreeLeaf.h"

SortTreeBranch::SortTreeBranch() {
	this->valueCount = 0;
}

SortTreeBranch::SortTreeBranch(SortTreeNode* initial) {
	this->left = initial;
	this->valueCount = 1;
}

SortTreeBranch::~SortTreeBranch() {
	if (this->valueCount > 1) {
		delete this->right;	
	}
	if (this->valueCount > 0) {
		delete this->left;	
	}
}

bool SortTreeBranch::insert(int value) {
	if (this->valueCount == 0) {

		this->left = new SortTreeLeaf(value);
		this->valueCount = 1;

	} else if (this->valueCount == 1) {

		if (value > this->left->getMaxValue()) {  
			// put this value on the empty right branch
			this->right = new SortTreeLeaf(value);
			this->valueCount = 2;
		} else {
			// move the left branch to the right branch and put this value on the left branch
			this->right = this->left;
			this->left = new SortTreeLeaf(value);
			this->valueCount = 2;
		}

	} else {
		SortTreeNode** nodeToPopulate;
		if (value > this->left->getMaxValue()) {
			// push this value onto the right branch
			nodeToPopulate = &(this->right);
		} else {
			// push this value onto the left branch
			nodeToPopulate = &(this->left);
		}
		if (!(*nodeToPopulate)->insert(value)) {
			// this branch was a leaf so push it down inside a new branch
			*nodeToPopulate = new SortTreeBranch(*nodeToPopulate);
			(*nodeToPopulate)->insert(value);
		}

	}
	return true;
}

void SortTreeBranch::print() {
	if (this->valueCount > 0) {
		this->left->print();
	}
	if (this->valueCount > 1) {
		this->right->print();
	}
}

int SortTreeBranch::getMinValue() {
	if (this->valueCount == 0) {
		return 0x7fffffff;
	} else {
		return this->left->getMinValue();
	}
}

int SortTreeBranch::getMaxValue() {
	if (this->valueCount == 0) {
		return 0x80000000;
	} else {
		return this->right->getMaxValue();
	}
}