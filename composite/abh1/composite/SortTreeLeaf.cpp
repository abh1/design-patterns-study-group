#include <stdio.h>

#include "SortTreeLeaf.h"

SortTreeLeaf::SortTreeLeaf(int value) {
	this->value = value;
}

bool SortTreeLeaf::insert(int value) {
	return false;
}

void SortTreeLeaf::print() {
	printf("%d ", this->value);
}

int SortTreeLeaf::getMinValue() {
	return this->value;
}

int SortTreeLeaf::getMaxValue() {
	return this->value;
}